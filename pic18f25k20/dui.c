
/********************************************************
 *   DUI - The driver on base UART   *
 *     author Tumanov Stanislav Aleksandrovich,          *
 *     Russia, Kirov city, maintumanov@mail.ru           *
 *     #define DUIPort - RS232 22.10.21
 *********************************************************/

#include <string.h>

#ifndef DUIReciveBufferSize
  #define DUIReciveBufferSize 44
#endif

#ifndef DUIUARTspeed
  #define DUIUARTspeed 115200
#endif

#ifndef DUIDataBufferSize
  #define DUIDataBufferSize 16
#endif

#ifndef DUIPort
  #define DUIPort DUIstream
#endif

typedef void(*DUIPOn)(unsigned int8, unsigned int8*);
typedef void (*DUIPInit)(void);

//-----------------------------------------------------------------//
//            DECLARATION                                          //
//-----------------------------------------------------------------//

//============= Main functions ===================
void DUIInit(); // Initialization of the serial port
void DUICheck(); // Verification and processing of incoming messages

//-- Functions for sending data --
void DUISendStateTrueFalse(unsigned int8  channel, int1 vol);
void DUISendStateOnOff(unsigned int8  channel, int1 vol); 
void DUISendStateYesNo(unsigned int8  channel, int1 vol);
void DUISendStateOpenClose(unsigned int8  channel, int1 vol); 
void DUISendUInt8(unsigned int8 channel, unsigned int8 vol);
void DUISendInt8(unsigned int8 channel, int8 vol);
void DUISendUInt16(unsigned int8 channel, unsigned int16 vol);
void DUISendInt16(unsigned int8 channel, int16 vol);
void DUISendUInt32(unsigned int8 channel, unsigned int32 vol);
void DUISendInt32(unsigned int8 channel, int32 vol);
void DUISendString(unsigned int8 channel, char *text);
void DUISendLog(char *text);
void DUISendEnableButton(unsigned int8 channel, unsigned int8 numButon, int1 enabled); 
void DUISendTemperature(unsigned int8 channel, unsigned int8 H, unsigned int8 L);
void DUITemperatureToRAW(unsigned int8 *temp_h, unsigned int8 *temp_l, float value);
void DUISendTemperatureF(unsigned int8 channel, float value);
void DUISendUInt8Bin(unsigned int8 channel, unsigned int8 vol);
//-- Functions for read data --
int8 DUIGetInt8();
int16 DUIGetInt16();
//void DUIReadString(unsigned int8 addr, const byte *data); //tested
unsigned int16 DUIReadUInt16(unsigned int8 addr, unsigned int8 *data);
int16 DUIReadInt16(unsigned int8 addr, unsigned int8 *data);
unsigned int32 DUIReadUInt32(unsigned int8 addr, unsigned int8 *data);
int32 DUIReadInt32(unsigned int8 addr, unsigned int8 *data);

//-- Functions forming the interface --
void DUIAddLabel(unsigned int8 channel, char *cname, char *suffix = 0);
void DUIAddLed(unsigned int8 channel, unsigned int8 colorIndex, char *cname);
void DUIAddChart(unsigned int8 channel, char *cname, char *suffix = 0);
void DUIAddLog(unsigned int8 channel, unsigned int8 lines, char *cname = 0);
//-----------------
void DUIAddEditUint8(unsigned int8 channel, unsigned int8 vol, unsigned int8 min, 
                     unsigned int8 max, char *cname, char *suffix = 0);
void DUIAddEditInt8(unsigned int8 channel, int8 vol, int8 min, 
                     int8 max, char *cname, char *suffix = 0);
void DUIAddEditUint16(unsigned int8 channel, unsigned int16 vol, unsigned int16 min, 
                     unsigned int16 max, char *cname, char *suffix = 0);  
void DUIAddEditInt16(unsigned int8 channel, int16 vol, int16 min, 
                     int16 max, char *cname, char *suffix = 0);
void DUIAddEditInt32(unsigned int8 channel, int32 vol, int32 min, 
                     int32 max, char *cname, char *suffix = 0);
//-----------------
void DUIAddSliderUint8(unsigned int8 channel, unsigned int8 vol, unsigned int8 min, 
                     unsigned int8 max, char *cname, char *suffix = 0);
void DUIAddSliderInt8(unsigned int8 channel, int8 vol, int8 min, 
                     int8 max, char *cname, char *suffix = 0);
void DUIAddSliderUint16(unsigned int8 channel, unsigned int16 vol, unsigned int16 min, 
                     unsigned int16 max, char *cname, char *suffix = 0);  
void DUIAddSliderInt16(unsigned int8 channel, int16 vol, int16 min, 
                     int16 max, char *cname, char *suffix = 0);
void DUIAddSliderInt32(unsigned int8 channel, int32 vol, int32 min, 
                     int32 max, char *cname, char *suffix = 0);
//-----------------
void DUIAddCheckBox(unsigned int8 channel, int1 vol, char *cname);
void DUIAddTextEdit(unsigned int8 channel, char * text, char *cname); 
void DUIAddButton(unsigned int8 channel, char *cname); 
void DUIAddTwoButtons(unsigned int8 channel, char *b1name, char *b2name); 
void DUIAddThreeButtons(unsigned int8 channel, char *b1name, char *b2name, char *b3name); 
void DUIAddEditTemperature(unsigned int8 channel, unsigned int8 H, unsigned int8 L, char *cname); 

//=========== Service functions ===================
void DUIDefaultRead(unsigned int8 channel, unsigned int8 *data);
void DUIDefaultFormInit(void);
unsigned int8 DUIRead();
void DUIMesAnalys();
void DUISendStart();
void DUISendByte(unsigned int8 vol);
void DUISendWord(unsigned int16 vol);
void DUISendInt(int16 vol);
void DUISendULong(unsigned int32 vol);
void DUISendLong(int32 vol);
void DUISendString(char *text);
void DUIAnalysis(unsigned int8 data);

//-----------------------------------------------------------------//
//            VARIABLES
//-----------------------------------------------------------------//

DUIPOn DUIOnReadData = DUIDefaultRead;
DUIPInit DUIOnFormInit = DUIDefaultFormInit;

int1 DUIPTransmitEnable = 0;

//BUFFER VARIABLES
unsigned int8 DUIReadBuffer[DUIReciveBufferSize];
unsigned int8 DUIBuffTail = 0;
unsigned int8 DUIBuffHead = 0;
unsigned int8 DUIBuffCount = 0;


//MESSAGE VARIABLES
unsigned int8 DUIPDataBuff[DUIDataBufferSize];
unsigned int8 DUIPAnalysisState = 0;
unsigned int8 DUIPDataPos = 0;
unsigned int8 DUIPDataSize;
unsigned int8 DUIPDataChannel;

//-----------------------------------------------------------------//
//            IMPLEMENTATION                                       //
//-----------------------------------------------------------------//
//============= Main functions =====================================
void DUIInit() {
  set_uart_speed(DUIUARTspeed, DUIPort);
  enable_interrupts(INT_RDA);
  enable_interrupts(GLOBAL);
  fputc(254, DUIPort);
}

//------------------------------------------------------------------
void DUICheck() {
  while (DUIBuffCount > 0) 
    DUIAnalysis(DUIRead());
}
//-- Functions for sending data ------------------------------------
void DUISendStateTrueFalse(unsigned int8  channel, int1 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(2);
   DUISendByte(0);
   DUISendByte(vol);
}
//------------------------------------------------------------------
void DUISendStateOnOff(unsigned int8  channel, int1 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(2);
   DUISendByte(1);
   DUISendByte(vol);
}
//------------------------------------------------------------------
void DUISendStateYesNo(unsigned int8  channel, int1 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(2);
   DUISendByte(2);
   DUISendByte(vol);
}
//------------------------------------------------------------------
void DUISendStateOpenClose(unsigned int8  channel, int1 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(2);
   DUISendByte(3);
   DUISendByte(vol);
}
//------------------------------------------------------------------
void DUISendUInt8(unsigned int8 channel, unsigned int8 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(2);
   DUISendByte(4);
   DUISendByte(vol);
}
//------------------------------------------------------------------
void DUISendInt8(unsigned int8 channel, int8 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(2);
   DUISendByte(5);
   DUISendByte(vol);
}
//------------------------------------------------------------------
void DUISendUInt16(unsigned int8 channel, unsigned int16 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(3);
   DUISendByte(6);
   DUISendWord(vol);
}
//------------------------------------------------------------------
void DUISendInt16(unsigned int8 channel, int16 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(3);
   DUISendByte(7);
   DUISendInt(vol);
}
//------------------------------------------------------------------
void DUISendUInt32(unsigned int8 channel, unsigned int32 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(5);
   DUISendByte(9);
   DUISendULong(vol);
}
//------------------------------------------------------------------
void DUISendInt32(unsigned int8 channel, int32 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(5);
   DUISendByte(8);
   DUISendLong(vol);
}
//------------------------------------------------------------------
void DUISendString(unsigned int8 channel, char *text) {
  if(!DUIPTransmitEnable) return;
  DUISendStart();
  DUISendByte(channel);
  DUISendByte(strlen(text) + 2);
  DUISendByte(13);
  DUISendString(text);
}
//------------------------------------------------------------------
void DUISendLog(char *text) {
  DUISendString(254, text);
}
//------------------------------------------------------------------
void DUISendEnableButton(unsigned int8 channel, unsigned int8 numButon, int1 enabled) {
   if(!DUIPTransmitEnable) return;
   unsigned int8  vol = numButon;
   if (enabled) vol = vol + 3;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(2);
   DUISendByte(4);
   DUISendByte(vol);
}

//------------------------------------------------------------------
void DUISendTemperature(unsigned int8 channel, unsigned int8 H, unsigned int8 L) {
  if(!DUIPTransmitEnable) return;
  DUISendStart();
  DUISendByte(channel);
  DUISendByte(3);
  DUISendByte(10);
  DUISendByte(H);  
  DUISendByte(L);  
}

void DUITemperatureToRAW(unsigned int8 *temp_h, unsigned int8 *temp_l, float value) {
    unsigned int16 cv;
  if (value < 0) {
      cv = (value / -0.00390625);
      cv --;
      cv = (0xFFFF ^ cv);
      *temp_h = (cv >> 8);
      *temp_l = (cv & 255);
  } else {
      cv = (value / 0.00390625);
      *temp_h = (cv >> 8);
      *temp_l = (cv & 255);
  }
}

void DUISendTemperatureF(unsigned int8 channel, float value) {
  unsigned int8 h, l;
  DUITemperatureToRAW(&h, &l, value);
  DUISendTemperature(channel, h, l);
}

//------------------------------------------------------------------
void DUISendUInt8Bin(unsigned int8 channel, unsigned int8 vol) {
   if(!DUIPTransmitEnable) return;
   DUISendStart();
   DUISendByte(channel);
   DUISendByte(2);
   DUISendByte(14);
   DUISendByte(vol);
}
//------------------------------------------------------------------
//-- Functions for read data ---------------------------------------
byte DUIGetInt8() {
  return DUIPDataBuff[1];
}
//------------------------------------------------------------------
int16 DUIGetInt16() {
  return make16(DUIPDataBuff[1], DUIPDataBuff[2]);
}
unsigned int16 DUIReadUInt16(unsigned int8 addr, unsigned int8 *data) {
  typedef volatile union {
    unsigned int16 i;
    unsigned int8 w[2];
  } t_c;
  t_c c;
  c.w[0] = data[addr];
  c.w[1] = data[addr + 1]; 
  return c.i;
}
//------------------------------------------------------------------
int16 DUIReadInt16(unsigned int8 addr, unsigned int8 *data) {
  typedef volatile union {
    int16 i;
    int8 w[2];
  } t_c;
  t_c c;
  c.w[0] = data[addr];
  c.w[1] = data[addr + 1]; 
  return c.i;
}
//------------------------------------------------------------------
unsigned int32 DUIReadUInt32(unsigned int8 addr, unsigned int8 *data) {
  typedef volatile union {
    unsigned int32 i;
    unsigned int8 w[4];
  } t_c;
  t_c c;
  c.w[0] = data[addr];
  c.w[1] = data[addr + 1]; 
  c.w[2] = data[addr + 2];
  c.w[3] = data[addr + 3];
  return c.i;
}
//------------------------------------------------------------------
int32 DUIReadInt32(unsigned int8 addr, unsigned int8 *data) {
  typedef volatile union {
    int32 i;
    unsigned int8 w[4];
  } t_c;
  t_c c;
  c.w[0] = data[addr];
  c.w[1] = data[addr + 1]; 
  c.w[2] = data[addr + 2];
  c.w[3] = data[addr + 3];
  return c.i;
}
//-- Functions forming the interface -------------------------------

void DUIAddLabel(unsigned int8 channel, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(1);
  DUISendByte(strlen(cname) + strlen(suffix) + 2);
  DUISendString(cname);
  DUISendString(suffix);
}
//------------------------------------------------------------------
void DUIAddLed(unsigned int8 channel, unsigned int8 colorIndex, char *cname) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(2);
  DUISendByte(strlen(cname) + 2);
  DUISendByte(colorIndex);
  DUISendString(cname);
}
//------------------------------------------------------------------
void DUIAddChart(unsigned int8 channel, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(3);
  DUISendByte(strlen(cname) + strlen(suffix) + 2);
  DUISendString(cname);
  DUISendString(suffix);
}

//------------------------------------------------------------------
void DUIAddLog(unsigned int8 channel, unsigned int8 lines, char *cname) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(10);
  DUISendByte(strlen(cname)+ 2);
  DUISendByte(lines);
  DUISendString(cname);
}
//------------------------------------------------------------------
void DUIAddEditUint8(unsigned int8 channel, unsigned int8 vol, unsigned int8 min, 
                     unsigned int8 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(4);
  DUISendByte(strlen(cname) + strlen(suffix) + 6);
  DUISendByte(4);
  DUISendByte(vol);
  DUISendByte(min);
  DUISendByte(max);
  DUISendString(cname);
  DUISendString(suffix);                   
}
//------------------------------------------------------------------
void DUIAddEditInt8(unsigned int8 channel, int8 vol, int8 min, 
                     int8 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(4);
  DUISendByte(strlen(cname) + strlen(suffix) + 6);
  DUISendByte(5);
  DUISendByte(vol);
  DUISendByte(min);
  DUISendByte(max);
  DUISendString(cname);
  DUISendString(suffix);                    
}
//------------------------------------------------------------------
void DUIAddEditUint16(unsigned int8 channel, unsigned int16 vol, unsigned int16 min, 
                     unsigned int16 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(4);
  DUISendByte(strlen(cname) + strlen(suffix) + 9);
  DUISendByte(6);
  DUISendWord(vol);
  DUISendWord(min);
  DUISendWord(max);
  DUISendString(cname);
  DUISendString(suffix);                    
}
//------------------------------------------------------------------
void DUIAddEditInt16(unsigned int8 channel, int16 vol, int16 min, 
                     int16 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(4);
  DUISendByte(strlen(cname) + strlen(suffix) + 9);
  DUISendByte(7);
  DUISendInt(vol);
  DUISendInt(min);
  DUISendInt(max);
  DUISendString(cname);
  DUISendString(suffix);                      
}
//------------------------------------------------------------------
void DUIAddEditInt32(unsigned int8 channel, int32 vol, int32 min, 
                     int32 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(4);
  DUISendByte(strlen(cname) + strlen(suffix) + 15);
  DUISendByte(8);
  DUISendLong(vol);
  DUISendLong(min);
  DUISendLong(max);
  DUISendString(cname);
  DUISendString(suffix);                      
}
//------------------------------------------------------------------
void DUIAddSliderUint8(unsigned int8 channel, unsigned int8 vol, unsigned int8 min, 
                     unsigned int8 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(5);
  DUISendByte(strlen(cname) + strlen(suffix) + 6);
  DUISendByte(4);
  DUISendByte(vol);
  DUISendByte(min);
  DUISendByte(max);
  DUISendString(cname);
  DUISendString(suffix);                   
}
//------------------------------------------------------------------
void DUIAddSliderInt8(unsigned int8 channel, int8 vol, int8 min, 
                     int8 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(5);
  DUISendByte(strlen(cname) + strlen(suffix) + 6);
  DUISendByte(5);
  DUISendByte(vol);
  DUISendByte(min);
  DUISendByte(max);
  DUISendString(cname);
  DUISendString(suffix);                    
}
//------------------------------------------------------------------
void DUIAddSliderUint16(unsigned int8 channel, unsigned int16 vol, unsigned int16 min, 
                     unsigned int16 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(5);
  DUISendByte(strlen(cname) + strlen(suffix) + 9);
  DUISendByte(6);
  DUISendWord(vol);
  DUISendWord(min);
  DUISendWord(max);
  DUISendString(cname);
  DUISendString(suffix);                    
}
//------------------------------------------------------------------
void DUIAddSliderInt16(unsigned int8 channel, int16 vol, int16 min, 
                     int16 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(5);
  DUISendByte(strlen(cname) + strlen(suffix) + 9);
  DUISendByte(7);
  DUISendInt(vol);
  DUISendInt(min);
  DUISendInt(max);
  DUISendString(cname);
  DUISendString(suffix);                      
}
//------------------------------------------------------------------
void DUIAddSliderInt32(unsigned int8 channel, int32 vol, int32 min, 
                     int32 max, char *cname, char *suffix) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(5);
  DUISendByte(strlen(cname) + strlen(suffix) + 15);
  DUISendByte(8);
  DUISendLong(vol);
  DUISendLong(min);
  DUISendLong(max);
  DUISendString(cname);
  DUISendString(suffix);                      
}
//------------------------------------------------------------------
void DUIAddCheckBox(unsigned int8 channel, int1 vol, char *cname) {
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(6);
  DUISendByte(strlen(cname) + 2);
  DUISendByte(8);
  DUISendByte(vol);
  DUISendString(cname);
}
//------------------------------------------------------------------
void DUIAddTextEdit(unsigned int8 channel, char * text, char *cname){
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(7);
  DUISendByte(strlen(cname) + strlen(text) + 2);
  DUISendString(text);
  DUISendString(cname);
}
//------------------------------------------------------------------ 
void DUIAddButton(unsigned int8 channel, char *cname){
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(8);
  DUISendByte(strlen(cname) + 2);
  DUISendByte(1);
  DUISendString(cname);
}
//------------------------------------------------------------------ 
void DUIAddTwoButtons(unsigned int8 channel, char *b1name, char *b2name){
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(8);
  DUISendByte(strlen(b1name) + strlen(b2name) + 3);
  DUISendByte(2);
  DUISendString(b1name);
  DUISendString(b2name);
}
//------------------------------------------------------------------ 
void DUIAddThreeButtons(unsigned int8 channel, char *b1name, char *b2name, char *b3name){
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(8);
  DUISendByte(strlen(b1name) + strlen(b2name) + strlen(b3name) + 3);
  DUISendByte(3);
  DUISendString(b1name);
  DUISendString(b2name);
  DUISendString(b3name);
}
//------------------------------------------------------------------ 
void DUIAddEditTemperature(unsigned int8 channel, unsigned int8 H, unsigned int8 L, char *cname){
  DUISendStart();
  DUISendByte(255);
  DUISendByte(channel);
  DUISendByte(9);
  DUISendByte(strlen(cname) + 3);
  DUISendByte(H);
  DUISendByte(L);
  DUISendString(cname);
}
//=========== Service functions ====================================
void DUIDefaultRead(unsigned int8 channel, unsigned int8 *data) {}
//------------------------------------------------------------------
void DUIDefaultFormInit() {}
//------------------------------------------------------------------
unsigned int8 DUIRead() {
  unsigned int8 res;
  res = DUIReadBuffer[DUIBuffHead];
  DUIBuffCount--;
  if (DUIBuffHead++ == DUIReciveBufferSize - 1)
    DUIBuffHead = 0;
  return (res);
}
//------------------------------------------------------------------
#int_RDA
void DUIRDA_isr() {
  do  {
    DUIReadBuffer[DUIBuffTail] = fgetc(DUIPort);
    DUIBuffCount++;
    if (DUIBuffTail++ == DUIReciveBufferSize - 1)
      DUIBuffTail = 0;
  }  while (kbhit());
}
//------------------------------------------------------------------
void DUIAnalysis(unsigned int8 data) {

  if (data == 253) {
    DUIPAnalysisState = 1;
    return ;
  }

  if (data == 255) DUIPAnalysisState += 50;
  else
  if (DUIPAnalysisState >= 50) {
    DUIPAnalysisState -= 50;
    data += 128;
  };
  
  switch (DUIPAnalysisState)
  {
    case 1:
      DUIPDataChannel = data;
      DUIPDataPos = 0;
      DUIPAnalysisState = 2;
      if (DUIPDataChannel == 255) {
          DUIPAnalysisState = 0;
          DUIPTransmitEnable = 1;
          (*DUIOnFormInit)();
        }
      if (DUIPDataChannel == 254) {
          DUIPAnalysisState = 0;
          DUIPTransmitEnable = 0;
        }
      break;
    case 2://datasize //          
        DUIPDataSize = data;
        DUIPDataPos = 0;
        if (DUIPDataSize == 0) DUIPAnalysisState = 4;
        else DUIPAnalysisState = 3;
        break;
   case 3:     
        DUIPDataBuff[DUIPDataPos] = data;
        DUIPDataPos++;
        if (DUIPDataPos == DUIPDataSize) {
        DUIPAnalysisState = 0;
        (*DUIOnReadData)(DUIPDataChannel, &DUIPDataBuff);         
      }
        break; 
  }
}
//-------------------------------------------------------------------------
void DUISendStart() {
  fputc(253, DUIPort);
}
//-------------------------------------------------------------------------
void DUISendByte(unsigned int8 vol) {
  if (vol < 253) fputc(vol, DUIPort);
  else {
    fputc(255, DUIPort);
    fputc(vol - 128, DUIPort);
  }
}
//-------------------------------------------------------------------------
void DUISendWord(int16 Data) {
  DUISendByte(Data >> 8);
  DUISendByte(Data &0xFF);
}
//-------------------------------------------------------------------------
void DUISendInt(int16 vol) {
   typedef volatile union {
      int16 i;
      unsigned int8 w[2];
    } t_c;
   t_c c;
   c.i = vol;
   DUISendByte(c.w[1]);
   DUISendByte(c.w[0]);
}
//-------------------------------------------------------------------------
void DUISendULong(unsigned int32 vol) {
   typedef volatile union {
      unsigned int32 i;
      unsigned int8 w[4];
    } t_c;
   t_c c;
   c.i = vol;
   DUISendByte(c.w[3]);
   DUISendByte(c.w[2]);
   DUISendByte(c.w[1]);
   DUISendByte(c.w[0]);
}
//-------------------------------------------------------------------------
void DUISendLong(int32 vol) {
   typedef volatile union {
      signed int32 i;
      unsigned int8 w[4];
    } t_c;
   t_c c;
   c.i = vol;
   DUISendByte(c.w[3]);
   DUISendByte(c.w[2]);
   DUISendByte(c.w[1]);
   DUISendByte(c.w[0]);
}
//-------------------------------------------------------------------------
void DUISendString(char *text) {
  int l = strlen(text);
  DUISendByte(l);
  for (int i = 0; i < l; i = i + 1) DUISendByte(text[i]);
}

