#include <main.h>
#include "dui.c"

// коофициент 1,59 (1мкс = 1,59 тактам таймера при частоте 20МГц)
// ================= IR MODULE ===============
 unsigned int16 ir_period_pr = 0;
 unsigned int16 ir_period_0 = 0;
 unsigned int16 ir_period_1 = 0;

// SNIR
unsigned int16 snir_pp1min = 1000;
unsigned int16 snir_pp1max = 1800;
unsigned int16 snir_ppmin = 1300;
unsigned int16 snir_ppmax = 2600;
unsigned int16 snir_p0min = 190;
unsigned int16 snir_p0max = 370;
unsigned int16 snir_p1min = 730;
unsigned int16 snir_p1max = 920;
unsigned int8 snir_nbyte = 0;
unsigned int8 snir_nbit = 0;
unsigned int8 snir_data[8];
unsigned int8 snir_state = 0;

// 0-NEC 1-SAMSUNG 2-SONY 3-RC6 4-sharp 5-tv 6-PT2262 7-EV1527 8-dooya
int8 ir_type = 8; 
unsigned int16 ir_pp1min = 5300;
unsigned int16 ir_pp1max = 6000;
unsigned int16 ir_ppmin = 2700;
unsigned int16 ir_ppmax = 3500;
unsigned int16 ir_pprmin = 1350;
unsigned int16 ir_pprmax = 1750;
unsigned int16 ir_p0min = 200;
unsigned int16 ir_p0max = 500;
unsigned int16 ir_p1min = 800;
unsigned int16 ir_p1max = 1300;
unsigned int8  ir_packsiaze = 32;
unsigned int16 ir_period = 0;
unsigned int8 ir_count = 0;
unsigned int8 ir_repeatcount = 0;
unsigned int8 ir_state = 0;
unsigned int1 ir_inv = 0;

typedef volatile union {
    unsigned int32 r;
    unsigned int8 w[4];
  } ir_code_template;
ir_code_template ir_readcode;
ir_code_template ir_code;

unsigned int1 ir_code_new = 0;
unsigned int8 ir_timer = 0;

#task(rate = 1 ms, max = 100 us)
void ir_module_task();

//TASK MESSAGE CHECK
#task(rate = 1 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);
void readedcode(void);

//-------------------------------------------------

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x03 );
    SET_TRIS_C( 0x84 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);

    //ir
    setup_timer_3(T3_INTERNAL | T3_DIV_BY_8);
    enable_interrupts(INT_EXT);
    ext_int_edge(H_TO_L);
    enable_interrupts(GLOBAL);
}

// ir check

#INT_EXT
void  EXT_isr(void) 
{
  output_bit(PWM, 1);
  output_bit(SCL, 0); //ch2
  output_bit(SDA, 0); //channel 3
  output_low(DB1);
  ir_period = get_timer3();
  
   if (!input(pINT)) { //H2L
      // =====  SNIR protocol ====
        if (snir_state == 2 && snir_nbyte < 8) {
            if (ir_period > snir_p0min && ir_period < snir_p0max) {
                snir_data[snir_nbyte] = snir_data[snir_nbyte] << 1;
                snir_nbit ++;
            } else if (ir_period > snir_p1min && ir_period < snir_p1max) { 
                snir_data[snir_nbyte] = snir_data[snir_nbyte] << 1;
                snir_data[snir_nbyte] ++;
                snir_nbit ++;
            } else snir_state = 0;
            if (snir_nbit == 8) {
                snir_nbit = 0;
                snir_nbyte ++;
            }
        } else if (snir_state == 1 && ir_period > snir_ppmin && ir_period < snir_ppmax) snir_state = 2;
    //================= ir =====================================
      switch (ir_type) {
         case 0: // =====  nec protocol ====
         case 1: // =====  samsung protocol ====
            if (ir_state == 2) {
                if (ir_period > ir_p0min && ir_period < ir_p0max) {
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_readcode.r ++;
                    ir_count ++;
                } else if (ir_period > ir_p1min && ir_period < ir_p1max) { 
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_count ++;
                } else ir_state = 0;
             } else if (ir_state == 1 && ir_period > ir_ppmin && ir_period < ir_ppmax) ir_state = 2;
             else if (ir_state == 1 && ir_period > ir_pprmin && ir_period < ir_pprmax) ir_state = 3;
         break;

         case 3:  // =====  rc6 protocol ====
         if (ir_state == 2) {
            if (ir_period > ir_p0min && ir_period < ir_p0max) {
                ir_readcode.r = ir_readcode.r << 1;
                ir_readcode.r ++;
                ir_count ++;
            } else if (ir_period > ir_p1min && ir_period < ir_p1max) {
                ir_count = ir_count + 2;
                ir_readcode.r = ir_readcode.r << 1;
                ir_readcode.r ++;
                ir_readcode.r = ir_readcode.r << 1;
                ir_readcode.r ++;
            } if (ir_period > ir_p1max && ir_period < 1000) {
                ir_count = ir_count + 3;
                ir_readcode.r = ir_readcode.r << 1;
                ir_readcode.r ++;
                ir_readcode.r = ir_readcode.r << 1;
                ir_readcode.r ++;
                ir_readcode.r = ir_readcode.r << 1;
                ir_readcode.r ++;
            } 
         } 
          break;
          case 4: // =====  sharp protocol ====
            if (ir_period > ir_p0min && ir_period < ir_p0max) {
                ir_readcode.r = ir_readcode.r << 1;
                ir_count ++;
            } else if (ir_period > ir_p1min && ir_period < ir_p1max) {
                ir_count ++;
                ir_readcode.r = ir_readcode.r << 1; 
                ir_readcode.r ++;
            } 
         break;
         case 5: // =====  tv protocol ====
            if (ir_state == 2) {
                if (ir_period > ir_p0min && ir_period < ir_p0max) {
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_count ++;
                } else if (ir_period > ir_p1min && ir_period < ir_p1max) {
                    ir_count ++;
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_readcode.r ++;
                }
            }
         break; 
         case 6: // =====  PT2262 protocol ====
        // output_high(DB1);
            if (ir_state == 0) {
                if (ir_period > ir_p0min && ir_period < ir_p0max) {
                    ir_inv = 0;
                   // output_high(DB1);
                } else if (ir_period > ir_p1min && ir_period < ir_p1max) { 
                    ir_inv = 1;
                   // output_high(DB1);
                }
             }
         break;
         case 7: // =====  EV1527 protocol ====
        // output_high(DB1);
            if (ir_state == 1) {
                if (ir_period > ir_p0min && ir_period < ir_p0max) {
                    ir_inv = 0;
                   //output_high(DB1);
                } else if (ir_period > ir_p1min && ir_period < ir_p1max) { 
                    ir_inv = 1;
                    //output_high(DB1);
                }
             }
         break;
         case 8: // =====  DOOYA protocol ====
        // output_high(DB1);
            // if (ir_state == 1) {
            //     if (ir_period > ir_p0min && ir_period < ir_p0max) {
            //         ir_inv = 0;
            //        //output_high(DB1);
            //     } else if (ir_period > ir_p1min && ir_period < ir_p1max) { 
            //         ir_inv = 1;
            //         //output_high(DB1);
            //     }
            //  }
         break;         
      }
      ext_int_edge(L_TO_H);
   } else {
            // =====  SNIR protocol ====
      if (ir_period > snir_pp1min && ir_period < snir_pp1max) {
            snir_nbit = 0;
            snir_nbyte = 0;
            snir_state = 1;
      }
    //================= ir =====================================
      switch (ir_type) { //L2H
         case 0:
         case 1:
            if (ir_period > ir_pp1min && ir_period < ir_pp1max) {
                ir_readcode.r = 0;
                ir_count = 0;
                ir_state = 1;
            }
            break;
         case 2: // =====  sony protocol ====
            if (ir_state == 2) {
                if (ir_period > ir_p0min && ir_period < ir_p0max) {
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_count ++;
                } else if (ir_period > ir_p1min && ir_period < ir_p1max) {
                    ir_count ++;
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_readcode.r ++;
                } 
             } else
              if (ir_period > ir_ppmin && ir_period < ir_ppmax) {
                  ir_readcode.r = 0;
                  ir_count = 0;
                  ir_state = 2;
             } 
         break;

         case 3: // =====  rc6 protocol ====
             if (ir_state == 2) {
                if (ir_period > ir_p0min && ir_period < ir_p0max) {
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_count ++;
                } else if (ir_period > ir_p1min && ir_period < ir_p1max) {
                    ir_count = ir_count + 2;
                    ir_readcode.r = ir_readcode.r << 2;
                } else if (ir_period > ir_p1max && ir_period < 980) {
                    ir_count = ir_count + 3;
                    ir_readcode.r = ir_readcode.r << 3; 
                } 
             } else  
                if (ir_period > ir_ppmin && ir_period < ir_ppmax) {
                  ir_readcode.r = 0;
                  ir_count = 0;
                  ir_state = 2;
                 }
             
         break;
         case 5: // =====  tv protocol ====
             if (ir_period > ir_ppmin && ir_period < ir_ppmax) {
                  ir_readcode.r = 0;
                  ir_count = 0;
                  ir_state = 2;
             } 
         break;
         case 6: // =====  PT2262 protocol ====
        // output_high(DB1);
            if (ir_state == 0) {
                if (ir_period > ir_p0min && ir_period < ir_p0max && ir_inv) {
                    ir_readcode.r = ir_readcode.r << 1;
                    
                    ir_period_0 = ir_period;
                    ir_count ++;
                } else if (ir_period > ir_p1min && ir_period < ir_p1max && !ir_inv) { 
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_readcode.r ++;
                    
                    ir_period_1 = ir_period;
                    ir_count ++;
                } else ir_count = 0;
             } 
             if (ir_period > ir_ppmin && ir_count == 24 && ir_inv) {
                 ir_state = 2;
                 ir_period_pr = ir_period;
             }
         break;
         case 7: // =====  EV1527 protocol ====
            if (ir_state == 1) {
                if (ir_period > ir_p0min && ir_period < ir_p0max && ir_inv) {
                    //output_high(DB1);
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_period_0 = ir_period;
                    ir_count ++;
                   // output_high(DB1);
                } else if (ir_period > ir_p1min && ir_period < ir_p1max && !ir_inv) { 
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_readcode.r ++;
                    ir_period_1 = ir_period;
                    ir_count ++;
                } else {
                    ir_count = 0;
                    ir_state = 0;
                }
             } 
             if (ir_period > ir_ppmin) {
                 ir_state = 1;
                 ir_period_pr = ir_period;
             }
         break;
         case 8: // =====  DOOYA ====
            if (ir_state == 1) {
                if (ir_period > ir_p0min && ir_period < ir_p0max && !ir_inv) {
                    output_high(DB1);
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_period_0 = ir_period;
                    ir_count ++;
                   // output_high(DB1);
                } else if (ir_period > ir_p1min && ir_period < ir_p1max && !ir_inv) { 
                    ir_readcode.r = ir_readcode.r << 1;
                    ir_readcode.r ++;
                    ir_period_1 = ir_period;
                    ir_count ++;
                }
                //  else {
                //     ir_count = 0;
                //     ir_state = 0;
                // }
             } 
             if (ir_period > ir_ppmin) {
                 ir_state = 1;
                 ir_period_pr = ir_period;
             }
         break;

      }
      ext_int_edge(H_TO_L); 
   }
   set_timer3(0);
   output_bit(LED2, !input(pINT));
   output_bit(PWM, 0);
   
}

void ir_module_task() {
switch (ir_type) {
  
    case 0: // ==== nec and 
    case 1: // samsung protocol
      ir_pp1min = 2700;
      ir_pp1max = 6000;
      ir_ppmin = 2700;
      ir_ppmax = 3500;
      ir_pprmin = 1350;
      ir_pprmax = 1750;
      ir_p0min = 230;
      ir_p0max = 450;
      ir_p1min = 900;
      ir_p1max = 1200;
      ir_packsiaze = 31;
      if (ir_state == 3) {
         if (ir_timer) ir_code_new = 1;
         ir_count = 0;
         ir_state = 0;
      }
      if (ir_count == ir_packsiaze) 
         if ((ir_readcode.w[0] + ir_readcode.w[1]) == 255) {
         ir_code.r = 0;
         ir_code.w[0] = ir_readcode.w[1];
         ir_code.w[1] = ir_readcode.w[2];
         ir_code.w[2] = ir_readcode.w[3];
         ir_code_new = 1;
         ir_count = 0;
         ir_state = 0;
      } 
      break;
    case 2:  // ==== sony protocol
      ir_ppmin = 1400;
      ir_ppmax = 1600;
      ir_p0min = 350;
      ir_p0max = 450;
      ir_p1min = 600;
      ir_p1max = 800;
      ir_packsiaze = 12;
      if (ir_count == ir_packsiaze) {
         ir_code.r = ir_readcode.r;
         ir_count = 0;
         ir_code_new = 1;
         ir_state = 0;
      } 
      break;
   case 3: // ==== rc6 protocol
      ir_ppmin = 1650;
      ir_ppmax = 1750;
      ir_p0min = 160;
      ir_p0max = 380;
      ir_p1min = 480;
      ir_p1max = 750;
      ir_packsiaze = 46;
      if (ir_count == 45 && get_timer3() > 1000) {
         ir_readcode.r = ir_readcode.r << 1;
         ir_readcode.r ++;
         ir_count ++;
      }
      
      if (ir_count == ir_packsiaze) {
          ir_readcode.w[2] = 0;
          ir_readcode.w[3] = 0;
         ir_code.r = ir_readcode.r;
         ir_readcode.r = 0;
         ir_code_new = 1;
         ir_count = 0;
         ir_state = 0;
      } 
      break;
  
  case 4: // sharp protocol
      if (ir_count > 0 && get_timer3() > 3000) {
         ir_readcode.r = 0;
         ir_count = 0;
      }
      ir_p0min = 350;
      ir_p0max = 500;
      ir_p1min = 800;
      ir_p1max = 1200;
      ir_packsiaze = 15;
      if (ir_count == ir_packsiaze) {
         if (bit_test(ir_readcode.r, 2)) {
         ir_code.r = ir_readcode.r;
         ir_code_new = 1;
         }
      ir_readcode.r = 0;
      ir_count = 0;
      ir_state = 0;
      } 
      break;

   case 5: // tv protocol
      ir_ppmin = 4000;
      ir_ppmax = 6000;
      ir_p0min = 200;
      ir_p0max = 500;
      ir_p1min = 750;
      ir_p1max = 1000;
      ir_packsiaze = 16;
      if (ir_count == ir_packsiaze) {
         ir_code.r = ir_readcode.r;
         ir_count = 0;
         ir_code_new = 1;
         ir_state = 0;
      } 
      break;  

   case 6: // PT2262
      ir_pp1min = 2700;
      ir_pp1max = 6000;
      ir_ppmin = 6000;
      ir_ppmax = 11000;
      ir_pprmin = 1350;
      ir_pprmax = 1750;
      ir_p0min = 600;
      ir_p0max = 900;
      ir_p1min = 190;
      ir_p1max = 350;
      ir_packsiaze = 24;

      if (ir_count >= ir_packsiaze) {
         ir_readcode.w[3] = 0;
         if (ir_code.r == ir_readcode.r) ir_code_new = 1;
         ir_code.r = ir_readcode.r;
         ir_readcode.r = 0;
         ir_count = 0;
         ir_state = 0;
      } 
      break;
 
   case 7: // EV1527
    ir_pp1min = 2700;
    ir_pp1max = 6000;
    ir_ppmin = 6000;
    ir_ppmax = 11000;
    ir_pprmin = 1350;
    ir_pprmax = 1750;
    ir_p0min = 500;
    ir_p0max = 800;
    ir_p1min = 100;
    ir_p1max = 320;
    ir_packsiaze = 24;

      if (ir_count >= ir_packsiaze) {
         ir_readcode.w[3] = 0;
         if (ir_code.r == ir_readcode.r) ir_code_new = 1;
         ir_code.r = ir_readcode.r;
         ir_readcode.r = 0;
         ir_count = 0;
         ir_state = 0;
      } 
      break;

   case 8: // DOOYA
    //  ir_pp1min = 6500;
    //  ir_pp1max = 9000;
      ir_ppmin = 6800;
      ir_ppmax = 7500;
   //   ir_pprmin = 1350;
   //   ir_pprmax = 1750;
      ir_p0min = 780;
      ir_p0max = 900;
      ir_p1min = 350;
      ir_p1max = 450;
      ir_packsiaze = 32;

      if (ir_count >= ir_packsiaze) {
         if (ir_code.r == ir_readcode.r) ir_code_new = 1;
         ir_code.r = ir_readcode.r;
         ir_readcode.r = 0;
         ir_count = 0;
         ir_state = 0;
      } 
      break;

   }    

    if (snir_nbyte == 8 && snir_nbit == 0) {
        output_bit(LED3, 1);
        char string[40];
        sprintf(string,"SNIR %X %X %X %X %X %X %X %X",
            snir_data[7], 
            snir_data[6],
            snir_data[5],
            snir_data[4],
            snir_data[3],
            snir_data[2],
            snir_data[1],
            snir_data[0] );
        DUISendString(7, string); 
        snir_state = 0;
        snir_nbyte = 0;
        snir_nbit = 0;
        ir_state = 0;
        output_bit(LED3, 0);
    }    
   
   if (ir_timer) ir_timer --;
   
   if (ir_code_new) {
      if (ir_timer == 0 && ir_code_new) {
        ir_repeatcount = 0;
        readedcode();
        
      } else if (ir_repeatcount < 200) ir_repeatcount ++;
      if (ir_repeatcount > 8) readedcode();
      ir_timer = 150;
      ir_code_new = 0;
   }

   output_bit(LED3, (ir_timer != 0));
}

void readedcode() {
    char string[40];
    DUISendUInt32(0, ir_code.r);
    sprintf(string,"CODE %X %X %X %X",
        ir_code.w[3], 
        ir_code.w[2],
        ir_code.w[1],
        ir_code.w[0]);
        DUISendString(7, string); 
}

void task_DUIcheck() {
   DUICheck();
   restart_wdt();
}

void formInit(void) {

   DUIAddEditUint8(5, ir_type, 0, 8, (char*)"protocol #");  
   DUIAddLabel(0, (char*)"Code");
   DUIAddLabel(1, (char*)"preambule");
   DUIAddLabel(2, (char*)"0");
   DUIAddLabel(3, (char*)"1");
   DUIAddLabel(4, (char*)"count");
   DUIAddButton(6, (char*)"get data"); 
   DUIAddLog(7, 40, (char*)"log");
}

void formRead(unsigned int8 channel, unsigned int8* data) {
   if (channel == 5) {
      ir_type = data[1];
   }
   
   if (channel == 6) {
      DUISendInt16(4, ir_count);
      DUISendUInt16(1, ir_period_pr);
      DUISendUInt16(2, ir_period_0);
      DUISendUInt16(3, ir_period_1);
      
   }
}

void main()
{
   initialization();
   rtos_run();
}
