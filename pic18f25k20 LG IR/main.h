#include <18F25K20.h>
#device ADC=10
#use delay(crystal=20000000)
#use rs232(baud=115200, parity=N, xmit=PIN_C6, rcv=PIN_C7, bits=8, stream = DUIstream, ERRORS)
#use rtos(timer=0, minor_cycle=1 ms)

#define LED1  PIN_A1
#define LED2  PIN_A2
#define LED3  PIN_A3

#define AN     PIN_A0
#define RST    PIN_B3
#define CS     PIN_B1
#define SCK    PIN_C3
#define MISO   PIN_C5
#define MOSI   PIN_C4

#define PWM    PIN_C2
#define RXIR   PIN_B0
#define RX     PIN_C7
#define TX     PIN_C6
#define SCL    PIN_C3
#define SDA    PIN_C4

#define DB1    PIN_B7
#define DB2    PIN_B6

// SNIR
#define snir_pp1min 8000
#define snir_pp1max 14400
#define snir_ppmin 10400
#define snir_ppmax 20800
#define snir_p0min 1520
#define snir_p0max 2960
#define snir_p1min 5840
#define snir_p1max 7360
// NEC LG AC
#define ir_pp1min 32000 //AGCpulse
#define ir_pp1max 48000
#define ir_ppmin 16000 //Lpause
#define ir_ppmax 25600
#define ir_pprmin 2240 //Dpulse
#define ir_pprmax 3360
#define ir_p0min 1840 //D0pause
#define ir_p0max 3600
#define ir_p1min 7200 //D1pause
#define ir_p1max 9600
#define ir_packsiaze 28