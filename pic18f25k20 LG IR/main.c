#include <main.h>
#include "dui.c"

// коофициент 1,59 (1мкс = 1,59 тактам таймера при частоте 20МГц)
// ================= IR MODULE ===============
unsigned int16 ir_period_pr = 0;
unsigned int16 ir_period_0 = 0;
unsigned int16 ir_period_1 = 0;

// SNIR
unsigned int8 snir_nbyte = 0;
unsigned int8 snir_nbit = 0;
unsigned int8 snir_data[8];
unsigned int8 snir_state = 0;

unsigned int16 ir_period = 0;
unsigned int8 ir_count = 0;
unsigned int8 ir_repeatcount = 0;
unsigned int8 ir_state = 0;
unsigned int1 ir_inv = 0;

typedef volatile union {
    unsigned int32 r;
    unsigned int8 w[4];
} ir_code_template;
ir_code_template ir_readcode;
ir_code_template ir_code;

unsigned int1 ir_code_new = 0;
unsigned int8 ir_timer = 0;

#task(rate = 1 ms, max = 100 us)
void ir_module_task();

//TASK MESSAGE CHECK
#task(rate = 1 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);
void readedcode(void);

//-------------------------------------------------

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x03 );
    SET_TRIS_C( 0x84 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);

    //ir
    setup_timer_3(T3_INTERNAL | T3_DIV_BY_1);
    enable_interrupts(INT_EXT);
    ext_int_edge(H_TO_L);
    enable_interrupts(GLOBAL);
}

// ir check

#INT_EXT
void  EXT_isr(void) 
{
    output_bit(PWM, 1);
    output_bit(SCL, 0); //ch2
    output_bit(SDA, 0); //channel 3
    output_low(DB1);
    ir_period = get_timer3();

    if (!input(RXIR)) { //H2L
        // =====  SNIR protocol ====
        if (snir_state == 2 && snir_nbyte < 8) {
            if (ir_period > snir_p0min && ir_period < snir_p0max) {
                snir_data[snir_nbyte] = snir_data[snir_nbyte] << 1;
                snir_nbit ++;
            } else if (ir_period > snir_p1min && ir_period < snir_p1max) {
                snir_data[snir_nbyte] = snir_data[snir_nbyte] << 1;
                snir_data[snir_nbyte] ++;
                snir_nbit ++;
            } else snir_state = 0;
            if (snir_nbit == 8) {
                snir_nbit = 0;
                snir_nbyte ++;
            }
        } else if (snir_state == 1 && ir_period > snir_ppmin && ir_period < snir_ppmax) snir_state = 2;
        // =====  nec LG protocol====
        if (ir_state == 2) {
            if (ir_period > ir_p0min && ir_period < ir_p0max) {
                ir_readcode.r = ir_readcode.r << 1;
                ir_count ++;
            } else if (ir_period > ir_p1min && ir_period < ir_p1max) {
                ir_readcode.r = ir_readcode.r << 1;
                ir_readcode.r ++;
                ir_count ++;
            } else ir_state = 0;

        } else if (ir_state == 1 && ir_period > ir_ppmin && ir_period < ir_ppmax) ir_state = 2;
        ext_int_edge(L_TO_H);
    } else { // --- L2H ---
        // =====  SNIR protocol ====
        if (ir_period > snir_pp1min && ir_period < snir_pp1max) {
            snir_nbit = 0;
            snir_nbyte = 0;
            snir_state = 1;
        }
        // ===== NEC LG protocol ====
        if (ir_period > ir_pp1min && ir_period < ir_pp1max) {
            ir_readcode.r = 0;
            ir_count = 0;
            ir_state = 1;
        }
        ext_int_edge(H_TO_L);
    }
    set_timer3(0);
    output_bit(LED2, !input(RXIR));
    output_bit(PWM, 0);

}

void ir_module_task() {

    if (ir_count == ir_packsiaze) {
        ir_code.r = 0;
        ir_code.w[0] = ir_readcode.w[0];
        ir_code.w[1] = ir_readcode.w[1];
        ir_code.w[2] = ir_readcode.w[2];
        ir_code.w[3] = ir_readcode.w[3];

        ir_code_new = 1;
        ir_count = 0;
        ir_state = 0;
    }

    if (snir_nbyte == 8 && snir_nbit == 0) {
        output_bit(LED3, 1);
        char string[40];
        sprintf(string,"SNIR %X %X %X %X %X %X %X %X",
                snir_data[7],
                snir_data[6],
                snir_data[5],
                snir_data[4],
                snir_data[3],
                snir_data[2],
                snir_data[1],
                snir_data[0] );
        DUISendString(7, string);
        snir_state = 0;
        snir_nbyte = 0;
        snir_nbit = 0;
        ir_state = 0;
        output_bit(LED3, 0);
    }

    if (ir_timer) ir_timer --;

    if (ir_code_new) {
        if (ir_timer == 0 && ir_code_new) {
            ir_repeatcount = 0;
            readedcode();

        } else if (ir_repeatcount < 200) ir_repeatcount ++;
        if (ir_repeatcount > 8) readedcode();
        ir_timer = 150;
        ir_code_new = 0;
    }

    output_bit(LED3, (ir_timer != 0));
}

void readedcode() {
    char string[40];
    DUISendUInt32(0, ir_code.r);
    sprintf(string,"CODE %X %X %X %X",
            ir_code.w[3],
            ir_code.w[2],
            ir_code.w[1],
            ir_code.w[0]);
    DUISendString(7, string);
    if ((ir_code.w[3] & 0x0F) == 0x08 && (ir_code.w[2] & 0xF0) == 0x80) {
        if ((ir_code.w[2] & 0x0F) == 0x0C) {
            DUISendUInt16(1, 255);
        } else if ((ir_code.w[1] & 0x0F) && (ir_code.w[2] & 0x0F) == 0x00) {
            DUISendUInt16(1, (ir_code.w[1] & 0x70) >> 4); //mode
            DUISendUInt16(2, (ir_code.w[1] & 0x0F) + 15); //temp 
            DUISendUInt16(3, (ir_code.w[0] & 0x70) >> 4); //fan


        }
    }

    

}

void task_DUIcheck() {
    DUICheck();
    restart_wdt();
}

void formInit(void) {
    char textp[]="protocol #";
    char suffix[]="";
    //DUIAddEditUint8(5, ir_type, 0, 7, textp, suffix);

    char text1[]="Code";
    DUIAddLabel(0, text1, suffix);
    char text3[]="preambule";
    DUIAddLabel(1, text3, suffix);
    char text4[]="0";
    DUIAddLabel(2, text4, suffix);
    char text5[]="1";
    DUIAddLabel(3, text5, suffix);
    char text6[]="count";
    DUIAddLabel(4, text6, suffix);
    char textb[]="get data";
    DUIAddButton(6, textb);
    char textl[]="log";
    DUIAddLog(7, 40, textl );
}

void formRead(unsigned int8 channel, unsigned int8* data) {
    if (channel == 5) {
        //ir_type = data[1];
    }

    if (channel == 6) {
        DUISendInt16(4, ir_count);
        DUISendUInt16(1, ir_period_pr);
        DUISendUInt16(2, ir_period_0);
        DUISendUInt16(3, ir_period_1);

    }
}

void main()
{
    initialization();
    rtos_run();
}
